import { combineReducers } from "redux";

import movieReducer from "./reducers/movieReducer";
import authReducer from "./reducers/authReducer";
const rootReducer = combineReducers({
  auth: authReducer,

  movie: movieReducer,
});

export default rootReducer;
