import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';

import { useForm } from 'react-hook-form';

import {
  SFlexContainer,
  SForm,
  StyledButton,
  StyledError,
  StyledInput,
  StyledTitle,
  IconStyleWrapper,
  SmallStyledTitle,
 
  SFlexDiv
} from '../components';

import { fetchUser } from '../actions';

const Login = () => {

  const userData = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const loginUser = (values) => dispatch(fetchUser(values));

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm();

  const navigate = useNavigate();

  const onSubmit = (data) => {
    loginUser(data);
  };

  //user is redirected after login
  useEffect(() => {
    userData.isLoggedIn ? navigate('home') : null;
  }, [userData.isLoggedIn]);

  return (
    <SFlexDiv>
      <SFlexContainer>
        <IconStyleWrapper />
        <StyledTitle>Login</StyledTitle>
        <SmallStyledTitle>Sign in to your account</SmallStyledTitle>
        <SForm onSubmit={handleSubmit(onSubmit)}>
     
          <StyledInput
            type=""
            placeholder="Email"
            {...register('email', { required: true })}
            validationFailed={errors.email}
          />

          <StyledInput
            type="password"
            placeholder="Password"
            {...register('password', { required: true })}
            validationFailed={errors.password}
          />

          {userData.error === 'Invalid username/password!' && (
            <StyledError>{userData.error}</StyledError>
          )}
          <StyledButton type="submit">Login</StyledButton>
        
        </SForm>
      </SFlexContainer>
    </SFlexDiv>
  );
};

export default Login;
