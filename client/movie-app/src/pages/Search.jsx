import { useState } from "react";

import { Typography } from "@mui/material";
import { Box } from "@mui/system";

import { MovieView, Navbar, SearchSection } from "../components";

import { API_URL } from "../common";

const Search = () => {
  const [query, setQuery] = useState("");
  const [movies, setMovies] = useState([]);

  const handleChange = (e) => {
    e.preventDefault();
    setQuery(e.target.value);
  };

  //find movies by query matching
  const fetchSearchMovie = (query) => {
    fetch(`${API_URL}search/shows?q=${query}`, {
      method: "GET",
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        console.log(data);
        setMovies(data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onSubmit = () => {
    fetchSearchMovie(query);
    setQuery("");
  };

  return (
    <div>
      <Navbar />
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
          gridRowGap: "30px",
          marginTop: "20px",
        }}
      >
        <SearchSection
          placeholder="Search for Movie"
          value={query}
          handleChange={handleChange}
          handleSearch={onSubmit}
        />

        {movies.length > 0 ? (
          movies.map((el) => (
            <MovieView
              photo={
                el.show.image
                  ? el.show.image.medium
                  : "https://st3.depositphotos.com/23594922/31822/v/600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg"
              }
              genres={el.show.genres}
              title={el.show.name}
              averageRuntime={el.show.averageRuntime}
              summary={el.show.summary}
              id={el.show.id}
              key={el.show.id}
            />
          ))
        ) : (
          <Typography>Find the best movies</Typography>
        )}
      </Box>
    </div>
  );
};

export default Search;
