import { useEffect, useState } from "react";

import { useParams } from "react-router-dom";

import { useDispatch, useSelector } from "react-redux";
import { fetchSingleMovie } from "../actions";

import { Rating, TextField, Typography, Box } from "@mui/material";

import { MovieView, Navbar } from "../components";

import { getReviewEndpoint, ratingEndPoint } from "../api";
import { token } from "../common";

const MovieDetails = () => {
  const { id } = useParams();
  const movieData = useSelector((state) => state.movie);

  const dispatch = useDispatch();

  const openSingleMovie = (id) => dispatch(fetchSingleMovie(id));

  const [rating, setRating] = useState(0);

  const [review, setReview] = useState("");

  useEffect(() => {
    openSingleMovie(id);

    //get comments for current movie
    fetch(getReviewEndpoint(id), {
      method: "GET",
      headers: {
        Authorization: "Bearer " + `${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        data[0].details?.content && setReview(data[0].details.content);
      })
      .catch((error) => {
        console.log(error);
      });
      
    //get rating for current movie
    fetch(ratingEndPoint(id), {
      method: "GET",
      headers: {
        Authorization: "Bearer " + `${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data[0].details?.rating);
        data[0].details?.rating && setRating(data[0].details.rating);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  //update rating for the movie
  const changeRating = (id, newValue) => {
    fetch(ratingEndPoint(id), {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + `${token}`,
      },
      body: JSON.stringify({ rating: newValue }),
    })
      .then((res) => res.json())
      .then((data) => {
        data[0].details?.rating && setRating(data[0].details.rating);
      })
      .catch((error) => {});
  };
  //update review for the movie
  const updateReview = (id, newValue) => {
    fetch(getReviewEndpoint(id), {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + `${token}`,
      },
      body: JSON.stringify({ content: newValue }),
    })
      .then((res) => res.json())
      .then((data) => {
        data[0].details?.content
          ? setReview(data[0].details.content)
          : setReview("");
      })
      .catch((error) => {});
  };

  return (
    <div>
      <Navbar />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          width: 750,
          height: 450,
          gridRowGap: "30px",
          marginLeft: "50px",
          marginTop: "20px",
        }}
      >
        <div key={movieData.movie.id}>
          <MovieView
            photo={
              movieData.movie.image
                ? movieData.movie.image.medium
                : "https://st3.depositphotos.com/23594922/31822/v/600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg"
            }
            genres={movieData.movie.genres}
            title={movieData.movie.name}
            averageRuntime={movieData.movie.averageRuntime}
            summary={movieData.movie.summary}
            key={movieData.movie.id}
            id={movieData.movie.id}
          />
        </div>
        <Typography gutterBottom variant="h5" component="div">
          Your Review
        </Typography>
        <Rating
          name="simple-controlled"
          value={rating}
          onChange={(event, newValue) => {
            changeRating(id, newValue);
          }}
        />
        <TextField
          id="outlined-textarea"
          color="secondary"
          placeholder="Your private notes and comments about the movie..."
          multiline
          display="inline-block"
          rows={5}
          value={review}
          onChange={(event) => {
            updateReview(id, event.target.value);
          }}
        />
      </Box>
    </div>
  );
};

export default MovieDetails;
