import { Favorites, Hero, Navbar } from "../components";

const Home = () => {
  return (
    <div>
      <Navbar />
      <Hero/>
      <Favorites/>
    </div>
  );
};

export default Home;
