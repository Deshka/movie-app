import Home from "./Home";
import Search from "./Search";
import MovieDetails from "./MovieDetails";
import NotFound from "./NotFound";
import Login from "./Login";
export { Home, Search, MovieDetails, NotFound, Login };
