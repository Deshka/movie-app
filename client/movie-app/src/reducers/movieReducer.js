import {
  SHOW_SINGLE_MOVIE,
  SHOW_SINGLE_MOVIE_ERROR,
} from "../actions/movieTypes";

const initialState = { movie: [], error: "" };

const reducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SHOW_SINGLE_MOVIE:
      return {
        ...state,
        movie: payload.movie,
      };
    case SHOW_SINGLE_MOVIE_ERROR:
      return {
        ...state,
        error: payload.error,
      };
    default:
      return state;
  }
};
export default reducer;
