import { API_URL } from "../common";
import { SHOW_SINGLE_MOVIE, SHOW_SINGLE_MOVIE_ERROR } from "./movieTypes";

const showSingleMovie = (data) => {
  return {
    type: SHOW_SINGLE_MOVIE,
    payload: { movie: data },
  };
};

const showSingleMovieError = (error) => {
  return {
    type: SHOW_SINGLE_MOVIE_ERROR,
    payload: error,
  };
};

export const fetchSingleMovie = (id) => {
  return (dispatch) => {
    fetch(`${API_URL}shows/${id}`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
     
       
        dispatch(showSingleMovie(data));
      })
      .catch((error) => {
        dispatch(showSingleMovieError(error));
      });
  };
};
