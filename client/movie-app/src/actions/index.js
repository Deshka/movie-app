import { fetchSingleMovie } from "./movieActions";
import { fetchUser } from "./authActions";

export { fetchSingleMovie, fetchUser };
