import decode from "jwt-decode";
import { Back_END_URL } from "../common";
import { LOGIN_FAIL, LOGIN_SUCCESS, LOGOUT } from "./authTypes";

const loginUserSuccess = (data) => {
 
  return {
    type: LOGIN_SUCCESS,
    payload: { user: data },
  };
};

const loginUserFailure = (error) => {
  return {
    type: LOGIN_FAIL,
    payload: { error: error },
  };
};

export const fetchUser = (values) => {
  return function (dispatch) {
    fetch(`${Back_END_URL}/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(values),
    })
      .then((data) => data.json())
      .then((data) => {
        if (data.token) {
          const user = decode(data.token);
          dispatch(loginUserSuccess({ user: user, token: data.token }));
        } else {
          dispatch(loginUserFailure(data.message));
        }
      })
      .catch((e) => {
        dispatch(loginUserFailure(e));
      });
  };
};

export const logoutUser = () => {
  return {
    type: LOGOUT,
  };
};
