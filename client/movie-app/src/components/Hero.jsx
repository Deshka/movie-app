import { Box, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";

import ReactPlayer from "react-player";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    height: "50vh",
    position: "relative",
    "& video": {
      objectFit: "cover",
    },
  },
  overlay: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
}));

const Hero = () => {
  const classes = useStyles();

  return (
    <section className={classes.root}>
      <ReactPlayer
        url={"https://www.youtube.com/watch?v=Y-r2t8RlXpU"}
        playing
        loop
        muted
        width="100%"
        height="100%"
      />
      <div className={classes.overlay}>
        <Box
          height="25%"
          display="flex"
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
          color="#fff"
        >
          <Typography variant="h3" component="h1" className={classes.title}>
            Welcome To Movie Star App
          </Typography>
          <Typography gutterBottom color="black" variant="h5" component="div">
            Find the best movies in our catalogue
          </Typography>
        </Box>
      </div>
    </section>
  );
};

export default Hero;
