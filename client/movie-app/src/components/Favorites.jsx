import { useEffect, useState } from "react";
import { useNavigate } from "react-router";

import { Box, Card, CardMedia, Typography } from "@mui/material";

import { getAllFavoritesEndP } from "../api";
import { token } from "../common";

const Favorites = () => {
  const [favorites, setFavorites] = useState([]);
  const navigate = useNavigate();

  //get all liked movies from current user
  useEffect(() => {
    fetch(getAllFavoritesEndP(), {
      method: "GET",
      headers: {
        Authorization: "Bearer " + `${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.error) {
          console.log(data.error);
        } else {
          setFavorites(data);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <div>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
          marginTop: "20px",
        }}
      >
        <Typography gutterBottom color="black" variant="h5" component="div">
          Your Favorites
        </Typography>
      </Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          flexDirection: "row",
          flexWrap: "wrap",
          margin: "20px",
        }}
      >
        {favorites.length ? (
          favorites.map((el, index) => (
            <Card
              key={index}
              sx={{
                display: "flex",
                flexDirection: "row",
                width: 350,
                height: 450,
                margin: 1,
                cursor: "pointer",
              }}
            >
              <CardMedia
                component="img"
                sx={{ width: 350 }}
                image={el.details.favorite.photo}
                onClick={() => navigate(`/movie/${+el.details.movie_id}`)}
              />
            </Card>
          ))
        ) : (
          <Typography>No movies added to favorites</Typography>
        )}
      </Box>
    </div>
  );
};

export default Favorites;
