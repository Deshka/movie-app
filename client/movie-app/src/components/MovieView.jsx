import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import {
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Button,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";

import { getFavStatusEndpoint } from "../api";
import { token } from "../common";

const MovieView = ({ id, photo, title, genres, averageRuntime, summary }) => {
  const navigate = useNavigate();
  const [favorite, setFavorite] = useState();

  useEffect(() => {
    //get if current movie(movies on search page or detail view) is liked
    fetch(getFavStatusEndpoint(id), {
      method: "GET",
      headers: {
        Authorization: "Bearer " + `${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.error) {
          console.log(data.error);
          setFavorite(false);
        } else {
          setFavorite(data[0].details?.favorite.is_liked);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  //add remove from favorites movies
  const changeFavStat = (id) => {
    fetch(getFavStatusEndpoint(id), {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + `${token}`,
      },
      body: JSON.stringify({ favorite: { is_liked: !favorite, photo: photo } }),
    })
      .then((res) => res.json())
      .then((data) => {
        data[0].details?.favorite &&
          setFavorite(data[0].details?.favorite.is_liked);
      });
  };

  return (
    <Card
      sx={{ display: "flex", flexDirection: "row", width: 750, height: 450 }}
    >
      <CardMedia
        component="img"
        sx={{ width: 200, cursor: "pointer" }}
        image={photo}
        onClick={() => navigate(`/movie/${id}`)}
      />
      <Box sx={{ display: "flex", flexDirection: "column" }}>
        <CardContent sx={{ flex: "1 0 auto" }}>
          <Typography gutterBottom variant="h5" component="div">
            {title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {genres?.join(",")} | {averageRuntime} minutes
          </Typography>

          <Typography variant="body2" color="text.secondary">
            {summary}
          </Typography>
        </CardContent>

        <CardActions>
          {favorite ? (
            <Button
              size="small"
              color="error"
              onClick={() => changeFavStat(id)}
            >
              Remove from Favorites
            </Button>
          ) : (
            <Button
              size="small"
              color="success"
              onClick={() => changeFavStat(id)}
            >
              Add to Favorites
            </Button>
          )}
        </CardActions>
      </Box>
    </Card>
  );
};

export default MovieView;
