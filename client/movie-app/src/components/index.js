import Favorites from "./Favorites";
import Hero from "./Hero";
import Navbar from "./Navbar";
import SearchSection from "./SearchSection";
import MovieView from "./MovieView";
import ProtectedRoute from "./ProtectedRoute";
import {
  SForm,
  IconStyleWrapper,
  StyledTitle,
  StyledInput,
  StyledError,
  SmallStyledTitle,
  StyledButton,
} from "./Form/Form/Form.styles";
import {
  SFlexContainer,
  SFlexDiv,
  FlexWrapper,
} from "./Form/Container/Container.styles";

export {
  Favorites,
  Hero,
  Navbar,
  SearchSection,
  MovieView,
  SForm,
  IconStyleWrapper,
  StyledTitle,
  StyledInput,
  StyledError,
  SmallStyledTitle,
  StyledButton,
  SFlexContainer,
  SFlexDiv,
  FlexWrapper,
  ProtectedRoute
};
