import { Link } from "react-router-dom";

import { useDispatch } from "react-redux";

import {
  AppBar,
  Box,
  Button,
  IconButton,
  Toolbar,
  Typography,
} from "@mui/material";

import { logoutUser } from "../actions/authActions";

const Navbar = () => {
  const dispatch = useDispatch();
  const handleLogout = () => dispatch(logoutUser());

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" color="secondary">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="open drawer"
            sx={{ mr: 2 }}
          ></IconButton>
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ flexGrow: 1, display: { xs: "none", sm: "block" } }}
          >
            My Movie App
          </Typography>
          <Link
            className="link"
            style={{ textDecoration: "none" }}
            to="/search"
          >
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{ marginRight: "25px" }}
              color="white"
            >
              Search
            </Typography>
          </Link>
          <Link className="link" style={{ textDecoration: "none" }} to="/">
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{ marginRight: "25px" }}
              color="white"
            >
              Home
            </Typography>
          </Link>
          <Button onClick={handleLogout}>
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{ marginRight: "25px" }}
              color="white"
            >
              Logout
            </Typography>
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
};
export default Navbar;
