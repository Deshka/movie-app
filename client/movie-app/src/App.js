import { BrowserRouter, Routes, Route } from "react-router-dom";

import { Home, Login, MovieDetails, NotFound, Search } from "./pages";


import { ProtectedRoute } from "./components";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="home" element={<ProtectedRoute component={Home} />}  />
        <Route
          path="movie/:id"
          element={<ProtectedRoute component={MovieDetails} />}
        />
        <Route path="search" element={<ProtectedRoute component={Search} />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
