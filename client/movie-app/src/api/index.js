import { Back_END_URL } from "../common";

export const getReviewEndpoint = (id) => `${Back_END_URL}/movie/${id}/review`;

export const getFavStatusEndpoint = (id) =>
  `${Back_END_URL}/movie/${id}/favorite`;

export const getAllFavoritesEndP = (id) => `${Back_END_URL}/movie/favorites`;

export const ratingEndPoint = (id) => `${Back_END_URL}/movie/${id}/rating`;
