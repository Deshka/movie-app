# React Movie APP
<br>


## Table of contents
   - [1. Description](#1-description)
   - [2. Project information](#2-project-information)
   - [3. Setup](#3-setup)
   - [4. Project Structure](#4-project-structure)
   - [5. Usage](#5-usage)
   - [6. Front-end](#6-front-end)
   - [7. Back-end](#7-back-end)
   - [8. ScreenShots](#8-screenshots)

<br>


### **1. Description**
<br> 


Application with user authentication where user can search for movies,like,rate and write notes for the movie.
<br>




### **2. Project information**
<br>

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages:  **React**,  **Redux**, **Express**, **ESLint**
- Libraries: **Material-UI**, **Styled-Components**

<br>
<br>

### **3. Setup**
   - 1. Clone/download repo
   - 2. yarn install (or npm install for npm) 

<br>
<br>

### **4. Project Structure**
- Project structure consists of two main parts - back-end (folder server) and front-end (folder client)
      
   **Front-end - folder (client)**
     - `src/App.js` - the entry point of the frontend project
     - `src/components` - contains shared components
     - `src/pages` - contains pages corresponding to routes specified in the App.js file
     - `src/actions` - actions
     - `src/reducers` - reducers
     - `src/api` - api endpoints url
     - `src/common` -reusable data

   <br>

   **Back-end - folder (server)**
     - `src/index.js` - the entry point of the backend project
     - `src/auth` - contain settings and logic for authentication 
     - `src/controllers` - contains all controller logic `auth`, `favorites`, `ratings`, `reviews`
     - `src/data` - contains the connection settings and functions for CRUD over movies and user data
     - `src/services` - contains all service logic



<br>
<br>

### **5. Usage**
   - `npm run start` - starts the project -App served @ http://localhost:3000
   - `npm run start server` - starts the server -App served @ http://localhost:5555

All commands:
- `npm run start` - starts the project
- `npm run lint` - run lint
- `npm run lint:fix` - fix lint errors
- `npm run start` -run server (user server folder)
- `npm run start:dev` -run server in dev mode (user server folder)
<br>
<br>


###  **6. Front-end**
   <br>

    Public routes
   - `Route - /` - directs to `Login` page

   <br>

    Private routes
   - `Route -home` - directs to `Home` page,only if the user is logged in
   - `Route - search` - directs to `Search` page where user can find favorites movies
   - `Route - movie/:id` - directs to `MovieDetails` render  detailed information for selected Movie

###  **7. Back-end**
  <br>  
     
    Endpoints
   - `Post: /login` - login current user
   - `GET: /movie/:id/favorite` - get favorites status of  movie
   - `PUT: /movie/:id/favorite` - update favorites status of the movie
   - `GET: /movie/favorites`    - get all liked by current usr movies
   - `GET: /movie/:id/rating`   - get rating of the current movie
   - `PUT: /movie/:id/rating` -   update rating of the current movie 
            - body: `{rating: Number}`, 
   - `GET: /movie/:id/review` -   get  created by current user review for the movie
   - `PUT: /movie/:id/review` -   updates the current user review for the movie
            - body: `{content: String}`
<br>
<br>
<br>

###  **8. Screenshots**

**Login View**

 ### **For Login you can use test@gmail.com pass:123456 or deshka@gmail.com pass:123456789**
<br>

![Login](./client/movie-app/public/Login.png)


<br>
<br>


<br>
<br>


**Home View**

![Home](./client/movie-app/public/Home.png)


<br>
<br>

<br>
<br>


**Search View**

![Search](./client/movie-app/public/Search.png)

<br>

![Search](./client/movie-app/public/Search1.png)

<br>
<br>

<br>
<br>


**Movie View**

![MovieView](./client/movie-app/public/MovieView.png)


