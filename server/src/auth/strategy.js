import passportJwt from "passport-jwt";
import dotenv from "dotenv";

const PRIVATE_KEY = dotenv.config().parsed.PRIVATE_KEY;

const options = {
  secretOrKey: PRIVATE_KEY,
  jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
};

const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
  const userData = {
    id: payload.id,
    email: payload.email,
  };

  done(null, userData);
});

export default jwtStrategy;
