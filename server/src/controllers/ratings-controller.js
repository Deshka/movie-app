import express from "express";

import { authMiddleware } from "../auth/auth-middleware.js";

import ratingsService from "../services/ratings-service.js";
import serviceErrors from "../services/service-errors.js";

import moviesData from "../data/movies.js";

const ratingsController = express.Router();

ratingsController
  .get("/movie/:id/rating", authMiddleware, async (req, res) => {
    const { id } = req.params;
    const user = req.user.id;

    const { error, rating } = await ratingsService.getRating(moviesData)(
      id,
      user
    );

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res
        .status(404)
        .send({ message: "No rating available fot that movie", error: error });
    } else {
      res.status(200).send(rating);
    }
  })
  .put("/movie/:id/rating", authMiddleware, async (req, res) => {
    const { id } = req.params;
    const { rating } = req.body;
    const user = req.user.id;
    const data = { id, rating };

    const { error, movie } = await ratingsService.updateRating(moviesData)(
      data,
      user
    );

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: "Data was not updated", error: error });
    } else {
      res.status(200).send(movie);
    }
  });

export default ratingsController;
