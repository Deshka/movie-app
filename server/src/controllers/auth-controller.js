import express from 'express';

import authService from "../services/auth-service.js";
import serviceErrors from "../services/service-errors.js";

import usersData  from "../data/users.js";

import createToken from '../auth/create-token.js';

const authController = express.Router();

authController
    .post('/login',  async (req, res) => {
      const { email, password } = req.body;

      const { error, user } = await authService.signInUser(usersData)(email, password);
    
      if (error === serviceErrors.INVALID_SIGNING) {
        res.status(400).send({ message: 'Invalid username/password!',error });
      } else {
        const payload = {
          id: user.user_id,
          email: user.email,
        };
        const token = createToken(payload);
        res.status(200).send({
          token: token,
        });
      }
    })

   
export default authController;
