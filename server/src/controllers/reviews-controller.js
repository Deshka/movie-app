import express from "express";

import { authMiddleware } from "../auth/auth-middleware.js";

import reviewsService from "../services/review-service.js";
import serviceErrors from "../services/service-errors.js";

import moviesData from "../data/movies.js";

const reviewsController = express.Router();

reviewsController
  .get("/movie/:id/review", authMiddleware, async (req, res) => {
    const { id } = req.params;
    const user = req.user.id;

    const { error, review } = await reviewsService.getReview(moviesData)(
      id,
      user
    );

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: "Reviews not found!", error: error });
    } else {
      res.status(200).send(review);
    }
  })
  .put("/movie/:id/review", authMiddleware, async (req, res) => {
    const { id } = req.params;
    const { content } = req.body;
    const user = req.user.id;
    const data = { id, content };

    const { error, review } = await reviewsService.updateReview(moviesData)(
      data,
      user
    );

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: "Reviews not found!", error: error });
    } else {
      res.status(200).send(review);
    }
  });
export default reviewsController;
