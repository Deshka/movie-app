import express from "express";

import favoritesService from "../services/favorites-service.js";
import serviceErrors from "../services/service-errors.js";

import moviesData from "../data/movies.js";

import { authMiddleware } from "../auth/auth-middleware.js";

const favoritesController = express.Router();

favoritesController
  .get("/movie/:id/favorite", authMiddleware, async (req, res) => {
    const { id } = req.params;

    const user = req.user.id;

    const { error, movie } = await favoritesService.getFavorite(moviesData)(
      id,
      user
    );

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res
        .status(404)
        .send({ message: "Movie is not db records", error: error });
    } else {
      res.status(200).send(movie);
    }
  })
  .put("/movie/:id/favorite", authMiddleware, async (req, res) => {
    const { id } = req.params;
    const { favorite } = req.body;
    const user = req.user.id;
    const data = { id, favorite };

    const { error, movie } = await favoritesService.updateFavoriteStat(
      moviesData
    )(data, user);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res
        .status(404)
        .send({ message: "Favorites status was not updated", error: error });
    } else {
      res.status(200).send(movie);
    }
  })
  .get("/movie/favorites", authMiddleware, async (req, res) => {
    const user = req.user.id;
    const { error, movies } = await favoritesService.getAllFavorites(
      moviesData
    )(user);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: "No Liked Movies", error: error });
    } else {
      res.status(200).send(movies);
    }
  });
export default favoritesController;
