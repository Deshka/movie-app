import { join, dirname } from "path";
import { Low, JSONFile } from "lowdb";
import { fileURLToPath } from "url";

const __dirname = dirname(fileURLToPath(import.meta.url));

const file = join(__dirname, "users.json");
const adapter = new JSONFile(file);
const db = new Low(adapter);

const getUser = async (email) => {
  await db.read();
  const userData = db.data.users.filter((el) => el.email === email);

  return userData;
};

export default {
  getUser,
};
