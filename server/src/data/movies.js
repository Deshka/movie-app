import { join, dirname } from "path";
import { Low, JSONFile } from "lowdb";
import { fileURLToPath } from "url";

const __dirname = dirname(fileURLToPath(import.meta.url));

const file = join(__dirname, "db.json");
const adapter = new JSONFile(file);
const db = new Low(adapter);

const getMovieData = async (id, user) => {
  await db.read();
  const userData = db.data.info.filter((el) => el.user_id === user);

  const data = userData.filter((el) => el.details.movie_id === +id);

  return data;
};

const updateReview = async (data, user) => {
  const review = await getMovieData(data.id, user);

  if (review[0]?.details?.movie_id) {
    review[0].details.content
      ? (review[0].details.content = data.content)
      : (review[0].details = { ...review[0].details, content: data.content });
  } else {
    db.data.info.push({
      user_id: user,
      details: {
        movie_id: +data.id,
        content: data.content,
      },
    });
  }
  await db.write();

  return await getMovieData(data.id, user);
};

const updateMovieStatus = async (data, user) => {
  const movie = await getMovieData(data.id, user);

  if (movie[0]?.details?.movie_id) {
    movie[0].details?.favorite?.is_liked
      ? (movie[0].details.favorite.is_liked = data.favorite.is_liked)
      : (movie[0].details = { ...movie[0].details, favorite: data.favorite });
  } else {
    db.data.info.push({
      user_id: user,
      details: {
        movie_id: +data.id,
        favorite: data.favorite,
      },
    });
  }
  await db.write();
  return await getMovieData(data.id, user);
};

const getAllFavorites = async (user) => {
  await db.read();
  const userData = db.data.info.filter((el) => el.user_id === user);

  const data = userData.filter((el) => el.details.favorite.is_liked === true);

  return data;
};

const updateRating = async (data, user) => {
  const movie = await getMovieData(data.id, user);

  if (movie[0]?.details?.movie_id) {
    movie[0].details?.rating
      ? (movie[0].details.rating = data.rating)
      : (movie[0].details = { ...movie[0].details, rating: data.rating });
  } else {
    db.data.info.push({
      user_id: user,
      details: {
        movie_id: +data.id,
        rating: data.rating,
      },
    });
  }
  await db.write();
  return await getMovieData(data.id, user);
};

export default {
  getMovieData,
  updateReview,
  updateMovieStatus,
  getAllFavorites,
  updateRating,
};
