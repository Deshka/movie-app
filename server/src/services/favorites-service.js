import serviceErrors from "./service-errors.js";

const getFavorite = (moviesData) => {
  return async (id, user) => {
    const movie = await moviesData.getMovieData(id, user);

    if (
      !movie[0] ||
      !movie[0].details.favorite ||
      !movie[0].details.favorite.is_liked
    ) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        review: null,
      };
    }

    return { error: null, movie };
  };
};

const updateFavoriteStat = (moviesData) => {
  return async (data, user) => {
    const movie = await moviesData.updateMovieStatus(data, user);
    if (!movie[0] || !movie[0].details) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        review: null,
      };
    }

    return { error: null, movie };
  };
};

const getAllFavorites = (moviesData) => {
  return async (user) => {
    const movies = await moviesData.getAllFavorites(user);

    if (!movies[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        review: null,
      };
    }

    return { error: null, movies };
  };
};
export default {
  getFavorite,
  updateFavoriteStat,
  updateFavoriteStat,
  getAllFavorites,
};
