import bcrypt from "bcrypt";

import serviceErrors from "./service-errors.js";

const signInUser = (usersData) => {
  return async (email, password) => {
    const user = await usersData.getUser(email);

    if (!user[0] || !(await bcrypt.compare(password, user[0].password))) {
      return {
        error: serviceErrors.INVALID_SIGNING,
        user: null,
      };
    }
    return {
      error: null,
      user: user[0],
    };
  };
};

export default {
  signInUser,
};
