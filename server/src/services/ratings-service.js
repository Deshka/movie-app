import serviceErrors from "./service-errors.js";

const getRating = (moviesData) => {
  return async (id, user) => {
    const rating = await moviesData.getMovieData(id, user);

    if (!rating[0] || !rating[0].details) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        review: null,
      };
    }

    return { error: null, rating };
  };
};

const updateRating = (moviesData) => {
  return async (data, user) => {
    const movie  = await moviesData.updateRating(data, user);
    if (!movie [0] || !movie [0].details) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        review: null,
      };
    }

    return { error: null, movie  };
  };
};

export default {
  getRating,
  updateRating,
};
