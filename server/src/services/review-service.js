import serviceErrors from "./service-errors.js";

const getReview = (moviesData) => {
  return async (id, user) => {
    const review = await moviesData.getMovieData(id, user);
  
    if (!review[0] || !review[0].details.content) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        review: null,
      };
    }

    return { error: null,  review };
  };
}; 
 
const updateReview = (moviesData) => {
  return async (data, user) => {
    const review = await moviesData.updateReview(data, user);

    if (!review[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        review: null,
      };
    }

    return { error: null,  review };
  };
};

export default {
  getReview,
  updateReview,
};
