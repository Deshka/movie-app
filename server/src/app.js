import express from "express";
import dotenv from "dotenv";
import helmet from "helmet";
import cors from "cors";
import reviewsController from "./controllers/reviews-controller.js";
import favoritesController from "./controllers/favorites-controller.js";
import ratingsController from "./controllers/ratings-controller.js";
import authController from "./controllers/auth-controller.js";
import passport from "passport";
import jwtStrategy from "./auth/strategy.js";

const app = express();

const config = dotenv.config().parsed;
const PORT = config.PORT;
passport.use(jwtStrategy);
app.use(cors());

app.use(passport.initialize());
app.use(helmet());
app.use(express.json());

app.use("/", reviewsController);
app.use("/", favoritesController);
app.use("/", ratingsController);
app.use("/", authController);
app.all("*", (req, res) =>
  res.status(404).send({ message: "Resource not found!" })
);

app.listen(PORT, console.log(`Listening on port ${PORT}...`));
